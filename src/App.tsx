import React, { useState } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import "./App.css";

import Favorites from "./components/Favorites";
import GogsList from "./components/GogsList";

// start of the application.
const App: React.FC = () => {
  // used to store the favorite dog urls.
  const [favoriteImages, setFavoriteImages] = useState<string[]>([]);

  // To add favorite URL to the list.
  const addFavoriteDog = (dogURL: string | undefined) => {
    if (dogURL) {
      setFavoriteImages((curremtList) => {
        curremtList.push(dogURL);
        return curremtList;
      });
    }
  };

  return (
    <Router>
      <Switch>
        <Route path="/favorites">
          <Favorites favorites={favoriteImages} />
        </Route>
        <Route path="/">
          <GogsList addFavoriteDog={addFavoriteDog} />
        </Route>
      </Switch>
    </Router>
  );
};

export default App;
