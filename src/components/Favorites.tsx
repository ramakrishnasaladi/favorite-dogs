import React from "react";
import { Link } from "react-router-dom";

interface Props {
  favorites: Array<string>;
}

// to display the favorite dog images.
const Favorites: React.FC<Props> = ({ favorites }) => {
  return (
    <div className="c-dog-list">
      <div className="c-dog-list__header">
        <h1>Your favorite dogs</h1>
        <Link className="c-link" to="/">
          Go Bacl
        </Link>
      </div>
      <div className="c-dog-list__body">
        {favorites.map((url: string) => (
          <img
            src={url}
            className="c-responsive-image"
            alt="loading please wait"
            key={url}
          />
        ))}
      </div>
    </div>
  );
};

export default Favorites;
