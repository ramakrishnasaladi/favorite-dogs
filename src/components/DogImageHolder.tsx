import React, { useEffect, useState } from "react";
import axios from "axios";

interface Props {
  handleChange: (imageUR: string | undefined) => void;
}

const DogImageHolder: React.FC<Props> = ({ handleChange }) => {
  // used to store the image URL.
  const [dogUrl, setDogUrl] = useState<string | undefined>(undefined);

  // call the API as soon as the component loads.
  useEffect(() => {
    axios
      .get("https://random.dog/woof.json")
      .then((response) => {
        setDogUrl(response.data.url);
      })
      .catch((err) => {
        console.log("error fetching image:", err);
      });
  }, []);

  return (
    <img
      src={dogUrl}
      className="c-responsive-image"
      alt="loading Please wait"
      onClick={() => handleChange(dogUrl)}
    />
  );
};

export default DogImageHolder;
