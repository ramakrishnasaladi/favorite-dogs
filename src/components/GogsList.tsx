import React, { useState } from "react";
import { Link } from "react-router-dom";

import DogImageHolder from "./DogImageHolder";

interface Props {
  addFavoriteDog: (imageUR: string | undefined) => void;
}

const GogsList: React.FC<Props> = ({ addFavoriteDog }) => {
  // as we are displaying 6 images at once.
  var [numberOfImages, setNumberofItems] = useState([1, 2, 3, 4, 5, 6]);

  // create 6 image holder elements.
  const loadImageHolder = () => {
    return numberOfImages.map((numberOfImages) => (
      <DogImageHolder
        handleChange={addFavoriteDog}
        key={numberOfImages.toString()}
      />
    ));
  };

  // used to store the list of image holder items.
  const [listItems, setListItems] = useState(() => loadImageHolder());

  // to load the page with 6 new images.
  const refresh = () => {
    setNumberofItems((currentListings) => {
      currentListings.map((value, index) => {
        currentListings[index] = value + 6;
      });
      return currentListings;
    });
    setListItems(() => loadImageHolder());
  };

  return (
    <div className="c-dog-list">
      <div className="c-dog-list__header">
        <h1>Choose the favorite by clicking on the image</h1>
        <Link className="c-link" to="/favorites">Favorites</Link>
      </div>
      <div className="c-dog-list__body">{listItems}</div>
      <div className="c-dog-list__footer">
        <button onClick={() => refresh()}>Next</button>
      </div>
    </div>
  );
};

export default GogsList;
